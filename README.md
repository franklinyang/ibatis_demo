##工程说明
此工程利用Spring提供的SqlMapClientTemplate简化了ibatis的开发，但是需要注意的是SqlMapClientTemplate已经过时了。
不需要找替代的，因为ibatis本身已经过时了，新工程推荐使用mybatis。
##数据源
此项目数据源采用hsql，运行于内存中，方便单元测试。
如果需要更换数据源运行此项目，除了更改Spring数据源配置，还需要
更换ibatis的Dao映射文件和数据库初始化脚本。

具体hsql支持sql标准参考官方文档 [http://hsqldb.org/web/hsqlDocsFrame.html](http://hsqldb.org/web/hsqlDocsFrame.html)

##写Demo遇到的问题
- 在写单元测试的时候，把@BeforeClass误写成@Before，造成每次执行一个测试方法都会重新执行数据库初始化脚本，至于其区别可自行google。
- 由于user表中主键设置自增长，所以需要在插入新的user记录返回新增数据的主键ID，这块每个数据库支持的方式不同，需要特别注意。
- 关于分页，ibatis提供一个脱离具体数据库的分页实现，但是通过阅读源码，该分页实现是利用ResultSet的absolute方法实现，该方法效率不如
直接根据具体的数据库写分页sql效率高，具体源码看参见com.ibatis.sqlmap.engine.execution.SqlExecutor，当然最佳实践是先在db server层
利用native sql分页，然后在使用层进行二次分页，保证效率与内存。