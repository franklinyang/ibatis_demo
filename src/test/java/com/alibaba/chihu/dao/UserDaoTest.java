package com.alibaba.chihu.dao;

import com.alibaba.chihu.domain.PageRequest;
import com.alibaba.chihu.domain.User;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

/*
http://blog.sina.com.cn/s/blog_3c62c21f01012l1f.html
*/

public class UserDaoTest{

    private static UserDao userDao;

    @BeforeClass
    public static void init(){
        //初始化
        ApplicationContext context = new ClassPathXmlApplicationContext("dao.xml");
        userDao=context.getBean(UserDao.class);
    }

    @Test
    public void testGetOne()
    {
        Assert.assertNotNull(userDao);

        assertUser(userDao.getOne(1), 1, "j2ee", "j2ee");
    }

    @Test
    public void testSaveAndDelete()
    {
        Assert.assertNotNull(userDao);
        long num=userDao.count();

        User user=new User("zy","123456");
        userDao.save(user);

        assertUser(user, "zy", "123456");
        assertUser(userDao.getOne(user.getId()), "zy", "123456");

        userDao.delete(3);
        Assert.assertEquals(num, userDao.count());
    }

    @Test
    public void testListAll()
    {
        Assert.assertNotNull(userDao);

        List<User> users=userDao.listAll();

        Assert.assertNotNull(users);
        Assert.assertEquals(userDao.count(), users.size());
    }

    @Test
    public void testListPage()
    {
        Assert.assertNotNull(userDao);

        List<User> users=userDao.list(new PageRequest(2,3));

        Assert.assertNotNull(users);
        Assert.assertEquals(3, users.size());

        assertUser(users.get(0), 4, "asd", "asd");
        assertUser(users.get(1), 5, "zxc", "zxc");
        assertUser(users.get(2), 6, "rty", "rty");
    }

    @Test
    public void testUpdate()
    {
        Assert.assertNotNull(userDao);

        User user=userDao.getOne(1);

        assertUser(user,"j2ee","j2ee");

        user.setUsername("sss");
        user.setPassword("sss");
        userDao.update(user);

        user=userDao.getOne(1);
        assertUser(user, "sss", "sss");

        user.setUsername("j2ee");
        user.setPassword("j2ee");
        userDao.update(user);
    }


    @Test(enabled = false)
    private void assertUser(User user,String username,String password)
    {
        Assert.assertNotNull(user);

        Assert.assertEquals(username, user.getUsername());
        Assert.assertEquals(password, user.getPassword());
    }

    @Test(enabled = false)
    private void assertUser(User user,long userId,String username,String password)
    {
        Assert.assertNotNull(user);

        Assert.assertEquals(userId, user.getId());
        Assert.assertEquals(username, user.getUsername());
        Assert.assertEquals(password, user.getPassword());
    }

}
