package com.alibaba.chihu.dao;

import com.alibaba.chihu.domain.User;

import java.util.List;

/**
 * 定义UserDao的接口
 */
public interface UserDao extends BaseDao<User>,PageDao<User>{
    long count();
    List<User> listAll();
}
