package com.alibaba.chihu.dao;

import com.alibaba.chihu.domain.PageRequest;

import java.util.List;


/**
 * Created by chihu.zy on 2015/7/20.
 */
public interface PageDao <T> {
   List<T> list(PageRequest req);
}
