package com.alibaba.chihu.dao.impl.ibatis;

import com.alibaba.chihu.dao.UserDao;
import com.alibaba.chihu.domain.PageRequest;
import com.alibaba.chihu.domain.User;
import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import java.util.Collection;
import java.util.List;

/**
 *
 * UserDAO的ibatis实现
 *
 * */
public class UserDaoImpl extends SqlMapClientDaoSupport implements UserDao {
    public User getOne(long id) {
        return (User) getSqlMapClientTemplate().queryForObject("getUserById",id);
    }

    public void delete(long id) {
        getSqlMapClientTemplate().delete("deleteUser",id);
    }

    public User save(User o) {
        getSqlMapClientTemplate().insert("insertUser",o);
        return o;
    }

    public void update(User o) {
        getSqlMapClientTemplate().update("updateUser",o);
    }

    public long count() {
        return  ((Long)getSqlMapClientTemplate().queryForObject("countUser")).longValue();
    }

    public List<User> listAll() {
        return getSqlMapClientTemplate().queryForList("listAllUser");
    }

    public List<User> list(PageRequest req) {
        int startIndex=0;

        if(req.getPage()>1)
        {
            startIndex=(req.getPage()-1)*req.getSize();
        }

        return  getSqlMapClientTemplate().queryForList("listAllUser",startIndex,req.getSize());
    }
}
