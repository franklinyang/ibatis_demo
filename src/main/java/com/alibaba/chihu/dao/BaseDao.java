package com.alibaba.chihu.dao;

/**
 *
 */
public interface BaseDao <T> {
    T getOne(long id);
    void delete(long id);
    T save(T o);
    void update(T o);
}
