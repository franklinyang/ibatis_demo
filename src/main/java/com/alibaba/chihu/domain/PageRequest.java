package com.alibaba.chihu.domain;

/**
 * 用于分页
 */
public class PageRequest {
    /**
     * 请求页序号，从1起
     * */
    private int page;
    /**
     * 每页记录数
     * */
    private int size;

    /**
     * @param page 请求页序号，从1起
     * @param size 每页记录数
     * */
    public PageRequest(int page, int size) {
        this.page = page;
        this.size = size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
